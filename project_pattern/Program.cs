﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project_pattern
{


    
/*    class CheckBox
    {
        private static string name = "This Check Box";
        public static void GetName()
        {
            Console.WriteLine(name);
        }
    }
    class RadioBut
    {
        private static string name = "This Radio Button";
        public static void GetName()
        {
            Console.WriteLine(name);
        }
    }
*/
    abstract class AbstractElement
    {
        public abstract void GetName();
    }
    abstract class UIFactory
    {
        public abstract AbstractElement CreateElement();
    //    public abstract RadioBut CreateRadioBut(); 
    }

    class RBFactory : UIFactory
    {
        public override AbstractElement CreateElement()
        {
            return new RBElement();
        }
    }
    class CBFactory : UIFactory
    {
        public override AbstractElement CreateElement()
        {
            return new CBElement();
        }
    }

    class RBElement : AbstractElement
    {
        public override void GetName()
        {
            Console.WriteLine("This is Radio button!");
        }
    }
    class CBElement : AbstractElement
    {
        public override void GetName()
        {
            Console.WriteLine("This is Check box!");
        }
    }

    class UI
    {
        private AbstractElement abstractElement;
        public UI(UIFactory UI_Factory)
        {
            abstractElement = UI_Factory.CreateElement();
        }
        public void GetName()
        {
            abstractElement.GetName();
        }
    }

    class CompressImpl
    {
        public void ZIP()
        {
            Console.WriteLine("zip");
        }
        public void RAR()
        {
            Console.WriteLine("rar");
        }
        public void JAR()
        {
            Console.WriteLine("jar");
        }
    }

    class Compress
    {
        protected CompressImpl _inst;
        protected Compress()
        {

        }
        public Compress(CompressImpl inst)
        {
            this._inst = inst;
        }
        public virtual void Comp() { }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            p.PatternAbstractFactory();
            p.PatternDecorator();
            p.PatternPatternMethod();
            p.PatternBridge();
            p.PatternVisitor();
            Console.Read();
        }

        List<UI> UIArray;
        UIFactory UIfact;
        UI ui;

        void PatternAbstractFactory()
        {
            Console.WriteLine("Start Main");
            UIfact = new RBFactory();
            ui = new UI(UIfact);
            ui.GetName();
       //     UIArray.Add(new UI(UIfact));
            UIfact = new CBFactory();
            ui = new UI(UIfact);
            ui.GetName();
       //     UIArray.Add(new UI(UIfact));

       //     Console.WriteLine();
       //     for (int i = 0; i < UIArray.Count;i++)
       //     {
       //         UIArray[i].GetName();
       //     }
                
           
        }

        class Decorator : AbstractElement
        {
            private AbstractElement abstractElement;

            public Decorator(UIFactory UI_Factory)
            {
                this.abstractElement = UI_Factory.CreateElement();
            }
            public override void GetName()
            {
                this.abstractElement.GetName();
                Console.WriteLine("+ Decorator logic!");
            }
            public AbstractElement AE
            {
                get
                {
                    return this.abstractElement;
                }
            }
        }
        AbstractElement ae;
        void PatternDecorator()
        {
            UIfact = new RBFactory();
            ae = new Decorator(UIfact);
            ae.GetName();
            ae = ((Decorator)ae).AE;
            ae.GetName();
            //     UIArray.Add(new UI(UIfact));
            UIfact = new CBFactory();
            ae = new Decorator(UIfact);
            ae.GetName();
        }

        class MakeShip
        {
            protected void GetWood()
            {
                Console.WriteLine("GetWood");
            }

            protected void ProccessWood()
            {
                Console.WriteLine("ProccessWood");
            }
            protected  virtual void BuildPlanShip() { }

            protected virtual void ProccessShip() { }
            protected void ConstractShip()
            {
                Console.WriteLine("ConstractShip");
            }
            public void BuildShip()
            {
                GetWood();
                ProccessWood();
                BuildPlanShip();
                ProccessShip();
                ConstractShip();
            }


        }

        class Frigate : MakeShip
        {
            protected override void BuildPlanShip()
            {
                base.BuildPlanShip();
                Console.WriteLine("BuildPlanShipFrigate");
            }
            protected override void ProccessShip()
            {
                base.ProccessShip();
                Console.WriteLine("ProccessShipFrigate");
            }
        }

        class Cliper : MakeShip
        {
            protected override void BuildPlanShip()
            {
                base.BuildPlanShip();
                Console.WriteLine("BuildPlanShipCliper");
            }
            protected override void ProccessShip()
            {
                base.ProccessShip();
                Console.WriteLine("ProccessShipCliper");
            }

        }
        void PatternPatternMethod()
        {
            Console.WriteLine("Pattern Method");
            MakeShip ship;
            ship = new Frigate();
            ship.BuildShip();
            ship = new Cliper();
            ship.BuildShip();
        }




        class RAR1 : Compress
        {
            public RAR1(CompressImpl p) : base(p)
            {

            }
            public override void Comp()
            {
                base.Comp();
                this._inst.RAR();
            }
        }

        class ZIP1 : Compress
        {
            public ZIP1(CompressImpl p) : base(p)
            {

            }
            public override void Comp()
            {
                base.Comp();
                this._inst.ZIP();
            }
        }

        class JAR1 : Compress
        {
            public JAR1(CompressImpl p) : base(p)
            {

            }
            public override void Comp()
            {
                base.Comp();
                this._inst.JAR();
            }
        }
        void PatternBridge()
        {
            Compress comp;
            comp = new RAR1(new CompressImpl());
            comp.Comp();
            comp = new ZIP1(new CompressImpl());
            comp.Comp();
            comp = new JAR1(new CompressImpl());
            comp.Comp();
        }



        class IStore
        {
            public virtual void accept(Visitor vt) { }
        }

        class Caf : IStore
        {
            public double sum = 50;

            public override void accept(Visitor vt)
            {
                vt.visite(this);
            }
        }
        class Par : IStore
        {
            public double sum = 50;

            public override void accept(Visitor vt)
            {
                vt.visite(this);
            }
        }
        class Avt : IStore
        {
            public double sum = 50;

            public override void accept(Visitor vt)
            {/**/
                vt.visite(this);
            }
        }

        class Visitor
        {
            public void visite(Caf k)
            {
                
                Console.Write("Caf before:");
                Console.WriteLine(k.sum);
                k.sum = k.sum * 0.5;
                Console.Write("Caf after:");
                Console.WriteLine(k.sum);
            }
            public void visite(Par p)
            {
                Console.Write("Par before:");
                Console.WriteLine(p.sum);
                p.sum = p.sum * 0.3;
                Console.Write("Par after:");
                Console.WriteLine(p.sum);
            }
            public void visite(Avt a)
            {
                Console.Write("Avt before:");
                Console.WriteLine(a.sum);
                a.sum = a.sum * 0.1;
                Console.Write("Avt after:");
                Console.WriteLine(a.sum);
            }
        }


        void PatternVisitor()
        {
            IStore store1 = new Caf();
            IStore store2 = new Par();
            IStore store3 = new Avt();

            Visitor visitor = new Visitor();

            store1.accept(visitor);
            store2.accept(visitor);
            store3.accept(visitor);

        }

    }
}
